package com.kaka.demo.marvelheroes.characters.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CharactersFeed {

    @SerializedName("offset")
    private int offset;

    @SerializedName("total")
    private int total;

    @SerializedName("results")
    private List<Character> characters;

    public int getOffset() {
        return offset;
    }

    public int getTotal() {
        return total;
    }

    public List<Character> getCharacters() {
        return characters;
    }
}
