package com.kaka.demo.marvelheroes;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

public final class JSONFromAsset {
    private static final String TAG = JSONFromAsset.class.getSimpleName();
    private static final String ENCODING = "UTF-8";

    private JSONFromAsset() {

    }

    public static String load(final AssetManager assetManager, final String fileName) {
        String json;
        try {
            InputStream is = assetManager.open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];

            is.read(buffer);
            is.close();

            json = new String(buffer, ENCODING);
        } catch (IOException ex) {
            Log.d(TAG, ex.getMessage());
            json = null;
        }

        return json;
    }
}
