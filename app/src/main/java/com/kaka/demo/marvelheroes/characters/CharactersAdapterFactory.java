package com.kaka.demo.marvelheroes.characters;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.kaka.demo.marvelheroes.MarvelAPI;
import com.kaka.demo.marvelheroes.characters.model.CharactersFeed;

import java.io.IOException;


public class CharactersAdapterFactory implements TypeAdapterFactory {

    @Override
    public <T> TypeAdapter<T> create(final Gson gson, final TypeToken<T> type) {
        if (!type.getType().equals(CharactersFeed.class)) {
            return null;
        }

        final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);

        return new TypeAdapter<T>() {
            @Override
            public void write(JsonWriter out, T value) throws IOException {
                delegate.write(out, value);
            }

            @Override
            public T read(JsonReader in) throws IOException {
                final JsonElement jsonTree = gson.fromJson(in, JsonElement.class);
                return delegate.fromJsonTree(((JsonObject) jsonTree).get(MarvelAPI.KEY_DATA));

            }
        };
    }
}
