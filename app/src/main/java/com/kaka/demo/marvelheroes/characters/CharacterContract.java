package com.kaka.demo.marvelheroes.characters;

import com.kaka.demo.marvelheroes.IPresenter;
import com.kaka.demo.marvelheroes.IView;

public interface CharacterContract {

    interface Presenter extends IPresenter {

    }

    interface View extends IView {

    }
}
