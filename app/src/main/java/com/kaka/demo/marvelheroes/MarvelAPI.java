package com.kaka.demo.marvelheroes;

public abstract class MarvelAPI {
    public static final String KEY_DATA = "data";
    public static final String KEY_RESULTS = "results";
}
