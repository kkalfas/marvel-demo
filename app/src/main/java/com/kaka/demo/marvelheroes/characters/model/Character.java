package com.kaka.demo.marvelheroes.characters.model;

import com.google.gson.annotations.SerializedName;
import com.kaka.demo.marvelheroes.comics.model.Comic;
import com.kaka.demo.marvelheroes.series.model.Serie;
import com.kaka.demo.marvelheroes.stories.model.Story;

import java.util.List;

public class Character {

    @SerializedName("id")
    private Integer id;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("thumbnail")
    private Thumbnail thumbnail;

    @SerializedName("resourceURI")
    private String resourceURI;

    @SerializedName("comics")
    private transient List<Comic> comics;

    @SerializedName("series")
    private transient List<Serie> series;

    @SerializedName("stories")
    private transient List<Story> stories;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public List<Comic> getComics() {
        return comics;
    }

    @Override
    public String toString() {
        return "id=" + id + "\n" +
                "name='" + name + "\n" +
                "description='" + description + "\n" +
                "thumbnail=" + thumbnail +
                "resourceURI='" + resourceURI + "\n" +
                "comics=" + comics + "\n" +
                "series=" + series + "\n" +
                "stories=" + stories;
    }

    public List<Serie> getSeries() {
        return series;
    }

    public List<Story> getStories() {
        return stories;
    }
}
