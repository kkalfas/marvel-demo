package com.kaka.demo.marvelheroes.characters.model;

import com.google.gson.annotations.SerializedName;

public class Thumbnail {

    @SerializedName("path")
    private String url;

    @SerializedName("extension")
    private String extension;

    public String getUrl() {
        return url;
    }

    public String getExtension() {
        return extension;
    }

    @Override
    public String toString() {
        return "url='" + url + "\n" +
                "extension='" + extension;
    }
}
